# Recipe APP API Proxy


NGINX proxy app for our recipe app API


## Usage


### environment variables

* `LISTEN_PORT` - port to listen on (default: `8000`)
* `APP_HOST` - host name of the app to forward requests to (default: `app`)
* `APP_PORT` - port of the app to forward requests to (default: `9000`)


### Usefull commands
* `aws-vault exec rolandas.simkus --duration=2h` (enables communication to aws for 2h)
* `sudo docker build -t proxy .` (starts a docker container from current directory with existing Dockerfile setup)
